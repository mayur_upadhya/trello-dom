const key = "49349806e0907179f12210048b5d0cc7";
const token ="db60a78152a74db5d68b81eaf6be6c16aa849ba28edbcc2bab237821552fd985";
const boardId = "5e9d329f7b671042945ca6d1";
const listId = "5ea0182f821d2114c7c5d154";

// async function getData(url) {
//   const response = await fetch(url)
//   const data = await response.json();
//   return data;
// }
// function datafetch() {
//     fetch(`https://api.trello.com/1/boards/${boardId}?actions=all&boardStars=none&cards=none&card_pluginData=false&checklists=none&customFields=false&fields=name%2Cdesc%2CdescData%2Cclosed%2CidOrganization%2Cpinned%2Curl%2CshortUrl%2Cprefs%2ClabelNames&lists=open&members=none&memberships=none&membersInvited=none&membersInvited_fields=all&pluginData=false&organization=false&organization_pluginData=false&myPrefs=false&tags=false&key=${key}&token=${token}`).then((boardData) => {
//         return boardData.json()
//     }).then((boardData) => {
//         boardName(boardData)
//     });

//     fetch(`https://api.trello.com/1/boards/${boardId}/cards/?key=${key}&token=${token}`)
//         .then((res) => {
//             return res.json()
//         }).then((cardData) => {
//             console.log(cardData)
//             loadCard(cardData)
//         })
// }
// start();
async function getBoardListApi() {
  let text = await fetch(
    `https://api.trello.com/1/boards/${boardId}
    /lists?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  );

  return await text.json();
}
async function createCardApi(value) {
  const text = await fetch(
    `https://api.trello.com/1/cards?idList=${listId}&name=${value}&key=${key}&token=${token}`,
    {
      method: "POST",
    }
  );

  return await text.json();
}

async function getListApiOnLoad(listId) {
  let text = await fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  );

  return await text.json();
}
async function createCheckListApi(cardId, value) {
  let text = await fetch(
    `https://api.trello.com/1/checklists?idCard=${cardId}&name=${value}&key=${key}&token=${token}`,
    {
      method: "POST",
    }
  );
  return await text.json();
}

async function createCheckListItemApi(checkListId, value) {
  let text = await fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${value}&key=${key}&token=${token}`,
    {
      method: "POST",
    }
  );

  return await text.json();
}

async function getCheckListApi(checkListId) {
  let text = await fetch(
    `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  );
  return await text.json();
}

async function getCheckListItemApi(checkListId, checkListItems) {
  let text = await fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkListItems}?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  );
  return await text.json();
}

function updateCheckListItemStateApi(cardId, checkListItemId, state) {
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkListItemId}?state=${state}&key=${key}&token=${token}`,
    {
      method: "PUT",
    }
  );
}
function updateCheckListApi(cardId, checkListItemId, updateCheckListValue) {
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkListItemId}?name=${updatedCheckListValue}&key=${key}&token=${token}`,
    {
      method: "PUT",
    }
  );
}

function deletecheckListItemApi(checkListItemId, checkListId) {
  fetch(
    `https://api.trello.com/1/checklists/${checkListId}/checkitems/${checkListItemId}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  );
}

function deletecheckListApi(checkListId) {
  fetch(
    `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  );
}

function deleteCardApi(cardId) {
  fetch(`https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`, {
    method: "DELETE",
  }).catch((err) => console.error(err));
}

const btn = document.querySelectorAll("ul")["0"];
btn.addEventListener("click", onClick);

async function creteList(text) {
    let listId = text[0].id;
    let listName = text[0].name;
    let title = document.getElementsByClassName("list-group")[0];
    title.firstElementChild.innerText = listName;
    return listId;
  }

function createCard(text) {
  let valueInput = document.getElementById("card-usr");
  let listName = document.querySelector("ul");
  newCard = document.createElement("div");
  newCard.className = "card bg-dark text-white";
  newCard.setAttribute("card-id", text.id);
  newCard.setAttribute("checkList-id", text.idChecklists);
  newCardBody = document.createElement("div");
  newCardBodyButton = document.createElement("BUTTON");
  newCardBodyButton.className = "btn btn-primary btn-sm float-right";
  newCardBodyButton.innerHTML = "Remove";
  newCardBody.className = "card-body";
  newCardBody.innerHTML = text.name;
  listName.insertBefore(newCard, valueInput);
  newCard.appendChild(newCardBody);
  newCardBody.appendChild(newCardBodyButton);
}

// function addCardsToList(dataOfCardsOnThisList, listData) {
//   for (let card = 0; card < dataOfCardsOnThisList.length; card++) {
//       addCard(listData, dataOfCardsOnThisList[card].name, dataOfCardsOnThisList[card].id);
//   }
// }
// async function cardAdd() {
//   let input = document.getElementById('input').value
//   document.getElementById('input').value = '';
//   await fetch(`https://api.trello.com/1/cards?name=${input}&pos=bottom&idList=5df4ee4e98bc885863d13dd9&keepFromSource=all&key=${key}&token=${token}`, {
//       method: 'post'
//   })
//   let cardList = document.getElementById("list");
//   let delteNameOfBoard = document.getElementById('input-content')
//   deletingNode(delteNameOfBoard)
//   deletingNode(cardList);
//   start();
// }

function craeteItemOnCard(target, cardId) {
  let modalHeading = document.getElementsByClassName("modal-title")[0];
  modalHeading.innerText = target.firstChild.data;
  target.setAttribute("data-toggle", "modal");
  target.setAttribute("data-target", "#myModal");
  let modalCard = document.getElementById("myModal");
  modalCard.setAttribute("cardId", cardId);
}
// function addChecklist(cardId) {
//   fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`)
//       .then((checklists) => {
//           return checklists.json()
//       })
//       .then((checklists) => {
//          // console.log(checklists)
//           addingChecklist(checklists)
//       })
// }
// async function createCheckList(cardId, checkListNAme, parentNode) {
//   await fetch(`https://api.trello.com/1/checklists?idCard=${cardId}&name=${checkListNAme}&pos=bottom&key=${key}&token=${token}`, {
//       method: 'POST'
// })
// deletingNode(parentNode)
// addChecklist(cardId)
// }
// function Listing(list) {
//   let listBox = document.createElement('div');
//   listBox.setAttribute('id', list.id);
//   listBox.classList.add('list-box');
//   let listLabel = document.createElement('div');
//   listLabel.classList.add('list-label');
//   let listName = document.createElement('span');
//   listName.classList.add('list-name');
//   listName.innerHTML = list.name;
//   let delBtn = document.createElement('button');
//   let ul = document.createElement('ul');
//   ul.classList.add('list');
//   ul.setAttribute('listId', list.id);
//   let addCardFormBtn = document.createElement('button');
//   addCardFormBtn.classList.add('btn', 'add-card');
//   addCardFormBtn.setAttribute('listId', list.id);
//   addCardFormBtn.innerHTML = 'Add New Card';
//   addCardFormBtn.addEventListener('click', displayCardAddForm);
//   let cardAddForm = document.createElement('div');
//   cardAddForm.classList.add('form-add-card');
//   cardAddForm.setAttribute('listId', list.id);
//   cancelBtn.classList.add('btn', 'cancel');
//   document.getElementById('list-holder').appendChild(listBox);
// }

function createCheckList(text) {
  let formCheck = document.createElement("div");
  formCheck.className = "form-check";
  formCheck.setAttribute("checkListId", text.id);
  let InputCheck = document.createElement("INPUT");
  InputCheck.setAttribute("type", "checkbox");
  InputCheck.setAttribute("checked", "");
  InputCheck.className = "form-check-input";
  InputCheck.id = "materialChecked";
  let labelName = document.createElement("label");
  labelName.className = "form-check-label";
  labelName.setAttribute("for", "materialChecked");
  labelName.innerHTML = text.name;
  let checKListDeleteButton = document.createElement("Button");
  checKListDeleteButton.setAttribute("type", "button");
  checKListDeleteButton.setAttribute("deleteCheckList", true);
  checKListDeleteButton.className = "close";
  checKListDeleteButton.innerHTML = "&times";
  formCheck.appendChild(InputCheck);
  formCheck.appendChild(labelName);
  labelName.appendChild(checKListDeleteButton);
  let addItem = document.createElement("button");
  addItem.setAttribute("type", "button");
  addItem.className = "btn btn-secondary";
  addItem.innerHTML = "Add an item";
  let inputCheckListItem = document.createElement("input");
  inputCheckListItem.setAttribute("type", "text");
  inputCheckListItem.setAttribute("placeholder", "Enter Checklist item Name");
  inputCheckListItem.className = "checkListItem";
  formCheck.appendChild(inputCheckListItem);
  formCheck.appendChild(addItem);
  let modalBody = document.getElementsByClassName("modal-body")["0"];
  modalBody.appendChild(formCheck);
}

function createCheckListItemOnLoad(text, checkListItems, checkListId) {
  let checkLists = document.getElementsByClassName("form-check");
  let checkList = "";
  for (let k = 0; k < checkLists.length; k++) {
    if (checkLists[k].getAttribute("checklistid") === checkListId) {
      checkList = checkLists[k];
    }
  }
  let targetListItem = checkList.querySelector("label");
  let value = text.name;
  let formCheck = document.createElement("div");
  formCheck.className = "form-check-checkList-item";
  formCheck.setAttribute("checkListItemId", checkListItems);
  let InputCheck = document.createElement("INPUT");
  InputCheck.setAttribute("type", "checkbox");
  if (text.state === "complete") {
    InputCheck.setAttribute("checked", "");
  }
  targettargettarget;
  InputCheck.className = "form-check-input";
  InputCheck.id = "materialChecked2";
  let labelName = document.createElement("label");
  labelName.className = "form-check-label";
  labelName.setAttribute("for", "materialChecked2");
  labelName.innerHTML = value;
  let checKListDeleteButton = document.createElement("Button");
  checKListDeleteButton.setAttribute("type", "button");
  checKListDeleteButton.setAttribute("deleteCheckListItem", true);
  checKListDeleteButton.className = "close";
  checKListDeleteButton.innerHTML = "&times";
  formCheck.appendChild(InputCheck);
  formCheck.appendChild(labelName);
  formCheck.appendChild(checKListDeleteButton);
  checkList.insertBefore(formCheck, targetListItem.nextSibling);
}

function createCheckListItemDom(target, text) {
  let value = target.previousSibling.value;
  let inputValue = target.previousSibling;
  inputValue.value = "";
  let formCheck = document.createElement("div");
  formCheck.className = "form-check-checkList-item";
  formCheck.setAttribute("checklistitemid", text.id);
  let InputCheck = document.createElement("INPUT");
  InputCheck.setAttribute("type", "checkbox");
  InputCheck.className = "form-check-input";
  InputCheck.id = "materialChecked2";
  let labelName = document.createElement("label");
  labelName.className = "form-check-label";
  labelName.setAttribute("for", "materialChecked2");
  labelName.innerHTML = value;
  let checKListDeleteButton = document.createElement("Button");
  checKListDeleteButton.setAttribute("type", "button");
  checKListDeleteButton.setAttribute("deleteCheckListItem", true);
  checKListDeleteButton.className = "close";
  checKListDeleteButton.innerHTML = "&times";
  formCheck.appendChild(InputCheck);
  formCheck.appendChild(labelName);
  formCheck.appendChild(checKListDeleteButton);
  let targetInput = target.parentNode;
  targetInput.insertBefore(formCheck, inputValue);
}

function createFormOnCheckListItemDom(target) {
  let formElement = document.createElement("form");
  let formGroup = document.createElement("div");
  formGroup.className = "form-group";
  let formGroupInputElement = document.createElement("input");
  formGroupInputElement.value = target.parentNode.childNodes[1].innerText;
  formGroupInputElement.setAttribute("type", "text");
  formGroupInputElement.className = "form-control";
  let formButton = document.createElement("Button");
  formButton.setAttribute("type", "submit");
  formButton.className = "btn btn-primary";
  formButton.innerText = "Save";
  formElement.appendChild(formGroup);
  formGroup.appendChild(formGroupInputElement);
  formElement.appendChild(formButton);
  formElement.appendChild(target.parentNode.childNodes[2]);
  target.parentNode.childNodes[1].replaceWith(formElement);
}

function updateCheckListFunDom(updatedCheckListValue) {
  let checkItemButton = target.parentNode.childNodes[2];
  let newInput = document.createElement("label");
  newInput.innerText = updatedCheckListValue;
  newInput.className = "form-check-label";
  newInput.setAttribute("for", "materialChecked2");
  target.parentNode.parentNode.appendChild(checkItemButton);
  target.parentNode.replaceWith(newInput);
}

function deleteCard(myObj) {
  myObj.remove();
}

function deletecheckListDom(target) {
  target.parentNode.parentNode.remove();
}

function deletecheckListItemDom(target) {
  target.parentNode.remove();
}

// async function BoradData(boardId) {
//   const response = await fetch(
//     `https://api.trello.com/1/boards/${boardId}?fields=name,url&key=${key}&token=${Token}`
//   );
//   const boardData = await response.json();
//   return boardData;
// }

// async function listData(boardId) {
//   const response = await fetch(
//     `https://api.trello.com/1/boards/${boardId}/lists?fields=name,url&key=${key}&token=${Token}`
//   );
//   const listsData = await response.json();
//   return listsData;
// }

// async function cardsData(listId) {
//   const response = await fetch(
//     `https://api.trello.com/1/lists/${listId}/cards?fields=name,url&key=${key}&token=${Token}`
//   );
//   const cardsData = await response.json();
//   return cardsData;
// }

// async function addCard(cardName, listId) {
//   const response = await fetch(
//     `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${key}&token=${Token}`,
//     { method: 'POST' }
//   );
//   const cardData = await response.json();
//   console.log(cardData);
//   return cardData;
// }



async function onClick(e) {
  target = e.target;
  console.log(target);

  if (target.id === "add-card") {
    e.preventDefault();
    let value = document.getElementById("card-usr").value;
    let text = await createCardApi(value);

    createCard(text);
  } else if (target.className.includes("card-body")) {
    let checkListId = target.parentNode.getAttribute("checkList-id");

    let cardId = target.parentNode.getAttribute("card-id");

   craeteItemOnCard(target, cardId);

    checkListId = checkListId.split(",");
    (async function loop() {
      for (let i = 0; i < checkListId.length; i++) {
        let text1 = await getCheckListApi(checkListId[i]);

        createCheckList(text1);

        let checkListItems = text1.checkItems;
        (async function loop() {
          for (let j = checkListItems.length - 1; j >= 0; j--) {
            let text = await getCheckListItemApi(
              checkListId[i],
              checkListItems[j].id
            );

            createCheckListItemOnLoad(
              text,

              checkListItems[j].id,
              checkListId[i]
            );
          }
        })();
      }
    })();
  } else if (target.id === "modal-button" || target.id === "myModal") {
    let formCheck = document.getElementsByClassName("form-check");

    for (let i = 0; i < formCheck.length; i++) {
      setTimeout((node) => node.remove(), 0, formCheck[i]);
    }
  } else if (target.id === "add-checkList") {
    let modalTarget = target.parentNode.parentNode.parentNode.parentNode;

    let cardId = modalTarget.getAttribute("cardId");

    let value = document.getElementById("checklists").value;
    let text = await createCheckListApi(cardId, value);

    createCheckList(text);
  } else if (target.innerHTML === "Add an item") {
    let checkListId = target.parentNode.getAttribute("checklistid");
    let value = target.previousSibling.value;
    let text = await createCheckListItemApi(checkListId, value);

    createCheckListItemDom(target, text);
  } else if (target.className === "form-check-label") {
    e.preventDefault();

    createFormOnCheckListItemDom(target);
  } else if (target.id === "materialChecked") {
    e.preventDefault();
  } else if (target.innerText === "Save") {
    e.preventDefault();
    let updatedCheckListValue = target.previousSibling.firstChild.value;
    let checkListItemId = target.parentNode.parentNode.getAttribute(
      "checklistitemid"
    );

    let cardId = target.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute(
      "cardid"
    );
    Fun(updatedCheckListValue);
    updateCheckListApi(cardId, checkListItemId, updateCheckListValue);
  } else if (target.id === "materialChecked2") {
    let checkListItemId = target.parentNode.getAttribute("checklistitemid");
    let cardId = target.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getAttribute(
      "cardid"
    );

    let state = "";

    if (!target.getAttribute("checked")) {
      target.setAttribute("checked", "true");
      state = "complete";
    } else {
      target.setAttribute("checked", "false");
      state = "incomplete";
    }

    updateCheckListItemStateApi(cardId, checkListItemId, state);
  } else if (target.getAttribute("deleteCheckListitem")) {
    e.preventDefault();
    let checkListItemId = target.parentNode.getAttribute("checklistitemid");
    let checkListId = target.parentNode.parentNode.getAttribute("checklistid");

    deletecheckListItemDom(target);
    deletecheckListItemApi(checkListItemId, checkListId);
  } else if (target.getAttribute("deleteCheckList")) {
    e.preventDefault();
    let checkListId = target.parentNode.parentNode.getAttribute("checkListId");

    deletecheckListDom(target);
    deletecheckListApi(checkListId);
  } else if (e.target.innerHTML === "Remove") {
    let myObj = e.target.parentNode.parentNode;
    let cardId = myObj.getAttribute("card-id");

    deleteCard(myObj);
    deleteCardApi(cardId);
  }
}

async function onLoad() {
  let text = await getBoardListApi();
  let listId = await creteList(text);
  let text1 = await getListApiOnLoad(listId);
  for (let i = 0; i < text1.length; i++) {
    createCard(text1[i]);
  }
}
onLoad();
